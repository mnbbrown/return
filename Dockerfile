FROM registry.matthewbrown.io/debian
COPY return return
ENV PORT 80
EXPOSE 80
ENTRYPOINT ["/return"]
