package main

import (
	"fmt"
	"net/http"
	"os"
)

var hostname string

func Handler(rw http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(rw, "Hello World from %s", hostname)
}

func main() {
	hostname, _ = os.Hostname()
	http.HandleFunc("/", Handler)
	http.ListenAndServe(":8080", nil)
}
