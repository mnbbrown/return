all: docker

test:
	go test -v

deploy:
	cat marathon.json | jq '.container.docker.image |= "registry.matthewbrown.io/return:$(TAG)"' | tee marathon.json
	cat marathon.json
	curl -vX POST http://$(MARATHON_HOST):8080/v2/apps -H "Content-Type: application/json" -d @marathon.json 
	
dockerise:
	docker build -t registry.matthewbrown.io/return .
	docker tag registry.matthewbrown.io/return:latest registry.matthewbrown.io/return:$(TAG)
	docker push registry.matthewbrown.io/return:$(TAG)
